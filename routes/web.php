<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\cursoController;

Route::get('/', homecontroller::class);

  Route::get('cursos', [cursoController::class, 'index']);


Route::get('cursos/create',[cursoController::class, 'create']);

Route::get('cursos/{curso}', [cursoController::class, 'show']);

/*Route::get('cursos/{curso}/{categoria?}', function($curso, $categoria = null) {

    if($categoria){
        return "Bienvenido al curso $curso, de la categoria $categoria";
    }else{
        return "Bienvenido al curso: $curso";
    }
    
});  */